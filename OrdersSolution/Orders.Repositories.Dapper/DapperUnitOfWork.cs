﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Orders.Repositories.Dapper.Repositories;
using Orders.Framework.Contracts.Factories;
using Orders.Framework.Contracts.Repositories;

namespace Orders.Repositories.Dapper
{
    public class DapperUnitOfWork: IUnitOfWork
    {
        private OrdersRepository _contactsRepository = null;
        private readonly IDbConnection _connection = null;
        private IDbTransaction _transaction = null;

        public DapperUnitOfWork()
        {
            //_connection = new SqlConnection(ConfigurationManager.ConnectionStrings["OrdersDBContextConnectionString"].ConnectionString);

            _connection = new SqlConnection("data source=LOCALHOST;initial catalog=OrdersDB;user id=tc8;password=tc8;");
            _connection.Open();
        }
        public void Dispose()
        {
            _connection.Close();
            _connection.Dispose();
        }

        public IOrdersRepository OrdersRepository => _contactsRepository ?? (_contactsRepository = new OrdersRepository(_connection, () => _transaction));
        public void BeginTransaction()
        {
            _transaction = _connection.BeginTransaction();
        }

        public void CommitChanges()
        {
            _transaction.Commit();
            _transaction = null;
        }

        public void RollbackChanges()
        {
            _transaction.Rollback();
            _transaction = null;
        }
    }
}
