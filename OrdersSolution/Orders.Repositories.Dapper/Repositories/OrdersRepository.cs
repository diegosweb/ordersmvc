﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Orders.Framework.Contracts.Repositories;
using Orders.Framework.Dtos;
using Orders.Framework.Entities;
using Dapper;

namespace Orders.Repositories.Dapper.Repositories
{
    public class OrdersRepository: BaseRepository, IOrdersRepository
    {
        public OrdersRepository(IDbConnection connection, Func<IDbTransaction> transaction) 
            : base(connection, transaction)
        {}

        #region IOrdersRepository

        public void Add(Order order)
        {
            const string queryOrder = "insert into order(order_first_name, order_last_name) values (@fn, @ln);"+
                                        "SELECT CAST(SCOPE_IDENTITY() as int)";
            const string queryEmails = "insert into order_email(order_id, email) values (@id, @email)";

            var id = Connection.Query<int>(queryOrder, new {fn = order.FirstName, ln = order.LastName}, this.Transaction);

            //Connection.Execute(queryEmails, order.Emails.Select(x => new {id, email = x}), this.Transaction);
        }

        public void Update(Order order)
        {
            const string queryUpdateOrder = "update order set order_first_name = @fn, order_last_name = @ln where order_id = @id";
            const string queryDeleteEmails = "delete from order_email where order_id = @id";
            const string queryAddEmails = "insert into order_email(order_id, email) values (@id, @email)";

            //Connection.Execute(queryUpdateOrder, new { id = order.OrderId, fn = order.FirstName, ln = order.LastName }, this.Transaction);
            //Connection.Execute(queryDeleteEmails, new { id = order.OrderId }, this.Transaction);
            //Connection.Execute(queryAddEmails, order.Emails.Select(x => new { id = order.OrderId, email = x }), this.Transaction);
        }

        public bool Remove(int id)
        {
            const string query = "delete from order where order_id = @value";

            return Connection.Execute(query, new { value = id }, this.Transaction) > 0;
        }

        public Order GetOrder(int id)
        {
            const string queryEmails = "select email from order_email where order_id = @value";

            var orderInfo = GetOrderInfo(id);

            if (orderInfo == null)
                return null;

            return new Order()
            {
                //OrderId = orderInfo.OrderId,
                FirstName = orderInfo.FirstName,
                LastName = orderInfo.LastName
                //Emails = Connection.Query<string>(queryEmails, new { value = orderInfo.OrderId }).ToList()
            };
        }

        public Order GetOrder(string email)
        {
            var id = GetOrderIdForEmail(email);

            return !id.HasValue
                ? null
                : GetOrder(id.Value);
        }

        public OrderDto GetOrderInfo(int id)
        {
            const string query = "select " +
                                 "  order_id as OrderId, " +
                                 "  order_first_name as FirstName, " +
                                 "  order_last_name as LastName " +
                                 " from order " +
                                 " where order_id = @value ";

            var parameters = new { value = id };

            return Connection
                .Query<OrderDto>(query, parameters)
                .SingleOrDefault();
        }

        public IEnumerable<OrderDto> GetList(string pattern = null)
        {
            const string  query = "select distinct " +
                        "  order.order_id as OrderId, " +
                        "  order.order_first_name as FirstName, " +
                        "  order.order_last_name as LastName " +
                        " from order " +
                        "  inner join order_email on order_email.order_id = order.order_id " +
                        " where order_first_name like @value " +
                        "  or order_last_name like @value " +
                        "  or order_email.email like @value ";

            var toSearch = string.IsNullOrWhiteSpace(pattern) ? string.Empty : pattern.Trim();
            var parameters = new { value = "%" + toSearch + "%"};

            return Connection
                .Query<OrderDto>(query, parameters)
                .ToList();
        }

        public int? GetOrderIdForEmail(string email)
        {
            const string query = "select order_id as OrderId from order_email where lower(email) = lower(@value)";
            var parameters = new { value = email };

            var order = Connection
                .Query<OrderDto>(query, parameters)
                .SingleOrDefault();

            return order == null ? null : (int?) order.OrderId;
        }

        public Order GetOrderByID(int id)
        {
            string sql = "SELECT * FROM Orders AS O INNER JOIN OrderProducts AS OP ON O.Id = OP.OrderId where O.Id = " + id.ToString();

            var orderDictionary = new Dictionary<int, Order>();


            var order = Connection.Query<Order, OrderProduct, Order>(
                sql,
                (order, orderProducts) =>
                {
                    Order orderEntry;

                    if (!orderDictionary.TryGetValue(order.Id, out orderEntry))
                    {
                        orderEntry = order;
                        orderEntry.OrderProducts = new List<OrderProduct>();
                        orderDictionary.Add(orderEntry.Id, orderEntry);
                    }

                    orderEntry.OrderProducts.Add(orderProducts);
                    return orderEntry;
                },
                splitOn: "ProductId")
            .Where(x => x.Id == id);


            return order.FirstOrDefault(x => x.Id == id);


        }

        public IEnumerable<OrderDto> GetList()
        {
            throw new NotImplementedException();
        }

        #endregion
    }
}
