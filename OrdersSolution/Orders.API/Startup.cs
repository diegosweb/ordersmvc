using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;
using Autofac;
using Autofac.Builder;
using Autofac.Extensions.DependencyInjection;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using Orders.API.Options;
using Orders.Framework.Contracts.Factories;
using Orders.Repositories.Dapper;
using Orders.Repositories.EF;
using Orders.Services;

namespace Orders.API
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; private set; }

        public ILifetimeScope AutofacContainer { get; private set; }

        public void ConfigureServices(IServiceCollection services)
        {
            services.AddSwaggerGen(x =>
            {
                x.SwaggerDoc("v1", new Microsoft.OpenApi.Models.OpenApiInfo { Title = "Orders API", Version = "v1" });

            });
            services.AddControllers();

            var builder = new ContainerBuilder();

            var container = builder.Build();
        }


        public void ConfigureContainer(ContainerBuilder builder)
        {
            builder.RegisterType<ServiceFactory>().As<IServiceFactory>().SingleInstance();
            //builder.RegisterType<EFUnitOfWork>().As<IUnitOfWork>().SingleInstance(); DapperUnitOfWork

            //change this line to use Dapper or EF repositories
            builder.RegisterType<EFUnitOfWork>().As<IUnitOfWork>().SingleInstance(); 
        }

        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            var swaggerOptions = new SwaggerOptions();
            Configuration.GetSection(nameof(SwaggerOptions)).Bind(swaggerOptions);

            app.UseSwagger(option =>
            {
                option.RouteTemplate = swaggerOptions.JsonRoute;
            }
            );

            app.UseSwaggerUI(option => {
                option.SwaggerEndpoint(swaggerOptions.UIEndpoint, swaggerOptions.Description);
            }
            );

            app.UseRouting();

            app.UseAuthorization();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });

            this.AutofacContainer = app.ApplicationServices.GetAutofacRoot();
        }
    }
}
