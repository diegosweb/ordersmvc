﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Orders.Framework.Contracts.Factories;

namespace Orders.API
{
    //[ApiController]
    public abstract class BaseController : ControllerBase
    {
        protected IServiceFactory Factory { get; private set; }
        public BaseController(IServiceFactory factory)
        {
            Factory = factory;
        }
    }
}
