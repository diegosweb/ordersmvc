﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System.Web;
using Orders.Framework.Contracts.Factories;
using Orders.Framework.Dtos;
using Orders.Framework.Entities;
using Orders.Framework.Exceptions;


namespace Orders.API.Controllers
{
    [Route("[controller]")]
    [ApiController]


    public class OrdersController : Orders.API.BaseController
    {
        public OrdersController(IServiceFactory factory)
            : base(factory)
        { }

        /*  public IEnumerable<OrderDto> Get()
          {
              return this.Factory.OrdersService.GetById(1);
          }*/


        [HttpGet("Get")]
        public IActionResult Get(int id)
        {
            var Order = this.Factory.OrdersService.GetById(id);

            if (Order == null)
                return NotFound();

            return Ok(Order);
        }

        [HttpGet("Post")]

        public IActionResult Post([FromBody] Order Order)
        {
            try
            {
                this.Factory.OrdersService.Add(Order);
                return Ok();
            }
            catch (BusinessException ex)
            {
                return BadRequest(ex.Message);
            }
            catch (EntityValidationException ex)
            {
                return BadRequest(string.Join(",", ex.Validations.Select(x => x.ErrorMessage)));
            }
            catch (Exception ex)
            {
                return BadRequest();
            }
        }


        [HttpPut("Put")]

        public IActionResult Put([FromBody] Order order)
        {
            try
            {
                this.Factory.OrdersService.Update(order);
                return Ok();
            }
            catch (BusinessException ex)
            {
                return BadRequest(ex.Message);
            }
            catch (EntityValidationException ex)
            {
                return BadRequest(string.Join(",", ex.Validations.Select(x => x.ErrorMessage)));
            }
            catch (Exception ex)
            {
                return BadRequest();
            }
        }

        [HttpDelete("Delete")]

        public IActionResult Delete(int id)
        {
            try
            {
                this.Factory.OrdersService.Remove(id);
                return Ok();
            }
            catch (BusinessException ex)
            {
                return BadRequest(ex.Message);
            }
            catch (Exception ex)
            {
                return BadRequest();
            }
        }
    }
}
