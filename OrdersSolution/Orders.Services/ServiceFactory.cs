﻿using Orders.Framework.Contracts.Factories;
using Orders.Framework.Contracts.Services;

namespace Orders.Services
{
    public class ServiceFactory: IServiceFactory
    {
        private OrdersService _ordersService = null;
        private readonly IUnitOfWork _factory = null;
        public ServiceFactory(IUnitOfWork factory)
        {
            _factory = factory;
        }
        public IOrdersService OrdersService => _ordersService ?? (_ordersService = new OrdersService(_factory));
    }
}
