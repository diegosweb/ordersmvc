﻿using Orders.Framework.Contracts.Factories;

namespace Orders.Services
{
    public abstract class BaseService
    {
        protected IUnitOfWork UnitOfWork { get; private set; }

        protected BaseService(IUnitOfWork uow)
        {
            UnitOfWork = uow;
        }
    }
}
