﻿using System.Collections.Generic;
using System.Linq;
using Orders.Framework.Contracts.Factories;
using Orders.Framework.Contracts.Services;
using Orders.Framework.Dtos;
using Orders.Framework.Entities;
using Orders.Framework.Exceptions;
using Orders.Framework.Helpers;

namespace Orders.Services
{
    public class OrdersService: BaseService, IOrdersService
    {
        public OrdersService(IUnitOfWork uow)
            :base(uow)
        {}
        public void Add(Order Order)
        {
            if (Order == null)
                throw new InvalidParametersException();

            var validations = Order.Validate().ToList();

            if (validations.Any())
                throw new EntityValidationException(validations);

            /*var emailInUse = Order.Emails.FirstOrDefault(x => UnitOfWork.OrdersRepository.GetOrderIdForEmail(x).HasValue);

            if (!string.IsNullOrEmpty(emailInUse))
                throw new BusinessException("Email is already in use.");

            Order.OrderId = 0;*/

            UnitOfWork.BeginTransaction();
            UnitOfWork.OrdersRepository.Add(Order);
            UnitOfWork.CommitChanges();
        }

        public void Update(Order Order)
        {
            /*if (Order == null || Order.OrderId <= 0)
                throw new InvalidParametersException();

            var validations = Order.Validate().ToList();

            if (validations.Any())
                throw new EntityValidationException(validations);

            if (UnitOfWork.OrdersRepository.GetOrderInfo(Order.OrderId) == null)
                throw new BusinessException("This order is invalid");

           
            if (!string.IsNullOrEmpty(emailInUse))
                throw new BusinessException("Email is already in use.");*/

            UnitOfWork.BeginTransaction();
            UnitOfWork.OrdersRepository.Update(Order);
            UnitOfWork.CommitChanges();
        }

        public void Remove(int id)
        {
            UnitOfWork.BeginTransaction();
            var removed = UnitOfWork.OrdersRepository.Remove(id);

            if (!removed)
                throw new BusinessException("This order is invalid");

            UnitOfWork.CommitChanges();
        }

        public Order GetById(int id)
        {
            return UnitOfWork.OrdersRepository.GetOrderByID(id);
        }



    }
}
