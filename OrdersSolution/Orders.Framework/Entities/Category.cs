﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Runtime.Serialization;
using System.Text;

namespace Orders.Framework.Entities
{
    [Serializable]
    [DataContract]
    class Category
    {
        [DataMember]
        public int Id
        {
            get;
            set;
        }

        [DataMember]
        [Required(AllowEmptyStrings = false, ErrorMessageResourceName = "NameRequired")]
        [MaxLength(100, ErrorMessageResourceName = "NameLength")]
        public string Name
        {
            get;
            set;
        }

        public IEnumerable<ProductCategory> ProductCategories
        {
            get;
            set;
        }
    }
}
