﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Runtime.Serialization;
using System.Text;

namespace Orders.Framework.Entities
{
    [Serializable]
    [DataContract]
   public class ProductCategory
    {
        [DataMember]
        [Required]
        public int ProductId { get; set; }


        [DataMember]
        [Required]
        public int CategoryId { get; set; }


    }
}
