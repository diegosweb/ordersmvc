﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Runtime.Serialization;
using System.ComponentModel.DataAnnotations;

namespace Orders.Framework.Entities
{
    [Serializable]
    [DataContract]
   public  class OrderProduct: BaseEntity
    {
        [DataMember]
        [Required(ErrorMessageResourceName = "OrderIdRequired")]
        public int OrderId { get; set; }

        [DataMember]
        [Required(ErrorMessageResourceName = "ProductIdRequired")]
        public int ProductId { get; set; }


        [DataMember]
        [Required(ErrorMessageResourceName = "QuantityRequired")]
        public int Quantity { get; set; }

        [DataMember]
        [Required(ErrorMessageResourceName = "PriceRequired")]
        public decimal Price { get; set; }


        [DataMember]
        public decimal Total { get; set; }
    }
}
