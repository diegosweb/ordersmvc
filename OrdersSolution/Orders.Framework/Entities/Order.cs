﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;
using Orders.Framework.Helpers;

namespace Orders.Framework.Entities
{
    [Serializable]
    [DataContract]
    public class Order: BaseEntity
    {
        [DataMember]
        public int Id { get; set; }

        [DataMember]
        [Required(AllowEmptyStrings = false, ErrorMessageResourceName = "FirstNameRequired")]
        [MaxLength(100, ErrorMessageResourceName = "FirstNameLength")]
        public string FirstName { get; set; }

        [DataMember]
        [Required(AllowEmptyStrings = false, ErrorMessageResourceName = "LastNameRequired")]
        [MaxLength(100, ErrorMessageResourceName = "LastNameLength")]
        public string LastName { get; set; }


        [DataMember]
        [MaxLength(100, ErrorMessageResourceName = "AddressLength")]
        public string Address { get; set; }


        [DataMember]
        [MaxLength(100, ErrorMessageResourceName = "CityLength")]
        public string City { get; set; }


        [DataMember]
        [MaxLength(100, ErrorMessageResourceName = "State")]
        public string State { get; set; }


        public IList<OrderProduct> OrderProducts { get; set; }


        public string FullName => $"{FirstName} {LastName}";
    }
}
