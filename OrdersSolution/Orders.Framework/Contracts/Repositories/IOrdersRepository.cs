﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Orders.Framework.Dtos;
using Orders.Framework.Entities;

namespace Orders.Framework.Contracts.Repositories
{
    public interface IOrdersRepository
    {
        void Add(Order Order);
        void Update(Order Order);
        bool Remove(int id);
        Order GetOrderByID(int id);
        OrderDto GetOrderInfo(int id);
        IEnumerable<OrderDto> GetList();
    }
}
