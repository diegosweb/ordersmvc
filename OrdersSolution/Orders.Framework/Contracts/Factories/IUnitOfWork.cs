﻿using System;
using Orders.Framework.Contracts.Repositories;

namespace Orders.Framework.Contracts.Factories
{
    public interface IUnitOfWork: IDisposable
    {
        IOrdersRepository OrdersRepository { get; }
        void BeginTransaction();
        void CommitChanges();
        void RollbackChanges();
    }
}
