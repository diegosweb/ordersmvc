﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Orders.Framework.Contracts.Repositories;
using Orders.Framework.Contracts.Services;

namespace Orders.Framework.Contracts.Factories
{
    public interface IServiceFactory
    {
        IOrdersService OrdersService { get; }
    }
}
