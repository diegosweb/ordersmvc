﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Orders.Framework.Dtos;
using Orders.Framework.Entities;
using Microsoft.SqlServer.Server;

namespace Orders.Framework.Contracts.Services
{
    public interface IOrdersService
    {
        void Add(Order Order);
        void Update(Order Order);
        void Remove(int id);
        Order GetById(int id);
        //IEnumerable<OrderDto> GetFiltered(string pattern = null);
    }
}
