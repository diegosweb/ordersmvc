﻿using System;
using System.Collections.Generic;
using System.Dynamic;
using System.Runtime.CompilerServices;
using System.Text;

namespace Orders.Framework.Dtos
{
   public  class ProductCategoryDto
    {
        public int ProductId { get; set; }
        public string ProductName { get; set; }

        public int CategoryId { get; set; }

        public string CategoryName { get; set; }
    }
}
