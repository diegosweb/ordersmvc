﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Orders.Framework.Dtos
{
    class OrderProductDto
    {
        public int OrderId { get; set; }
        public string OrderName { get; set; }
        public int ProductId { get; set; }
        public string ProductName { get; set; }

    }
}
