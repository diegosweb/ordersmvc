﻿using System;
using System.Linq;
using Microsoft.EntityFrameworkCore;
using Orders.Framework.Contracts.Factories;
using Orders.Framework.Contracts.Repositories;
using Orders.Repositories.EF.Contexts;
using Orders.Repositories.EF.Repositories;

namespace Orders.Repositories.EF
{
    public class EFUnitOfWork: IUnitOfWork
    {
        private OrdersDBContext _context = null;
        private OrdersRepository _contactsRepository = null;
        public EFUnitOfWork()
        {
            _context = new OrdersDBContext();
        }
        public IOrdersRepository OrdersRepository => _contactsRepository ?? (_contactsRepository = new OrdersRepository(_context));


        public void BeginTransaction()
        {
            //We don't need it on EF
        }

        public void CommitChanges()
        {
            if (!_context.ChangeTracker.HasChanges())
                return;

            try
            {
                _context.SaveChanges();
            }
            catch (Exception)
            {
                RollbackChanges();
                throw;
            }
        }

        public void RollbackChanges()
        {
            var changedEntries = _context.ChangeTracker.Entries()
                .Where(x => x.State != EntityState.Unchanged)
                .ToList();

            foreach (var entry in changedEntries)
            {
                switch (entry.State)
                {
                    case EntityState.Modified:
                        entry.CurrentValues.SetValues(entry.OriginalValues);
                        entry.State = EntityState.Unchanged;
                        break;
                    case EntityState.Added:
                        entry.State = EntityState.Detached;
                        break;
                    case EntityState.Deleted:
                        entry.State = EntityState.Unchanged;
                        break;
                }
            }
        }

        public void Dispose()
        {
            if (_context == null)
                return;

            _context.Dispose();
            _context = null;
        }
    }
}
