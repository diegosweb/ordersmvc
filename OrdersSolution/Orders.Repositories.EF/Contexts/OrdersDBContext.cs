﻿using System;
using System.Data;
using System.Linq;
using System.Linq.Expressions;
using System.ComponentModel;
using System.Reflection;
using System.Data.Common;
using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.EntityFrameworkCore.Internal;
using Microsoft.EntityFrameworkCore.Metadata;
using Orders.Repositories.EF.Models;

namespace Orders.Repositories.EF.Contexts
{

    public partial class OrdersDBContext : DbContext
    {

        public OrdersDBContext() :
            base()
        {
            OnCreated();
        }

        public OrdersDBContext(DbContextOptions<OrdersDBContext> options) :
            base(options)
        {
            OnCreated();
        }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (!optionsBuilder.IsConfigured)
            {
                if (!optionsBuilder.Options.Extensions.OfType<RelationalOptionsExtension>().Any(ext => !string.IsNullOrEmpty(ext.ConnectionString) || ext.Connection != null))
                  optionsBuilder.UseSqlServer(GetConnectionString("OrdersDBContextConnectionString"));
            }
            CustomizeConfiguration(ref optionsBuilder);
            base.OnConfiguring(optionsBuilder);
        }

        private static string GetConnectionString(string connectionStringName)
        {
            System.Configuration.ConnectionStringSettings connectionStringSettings = System.Configuration.ConfigurationManager.ConnectionStrings[connectionStringName];
            if (connectionStringSettings == null)
                //throw new InvalidOperationException("Connection string \"" + connectionStringName +"\" could not be found in the configuration file.");
                return "data source=LOCALHOST; initial catalog=OrdersDB; user id=tc8; password=tc8";

            return connectionStringSettings.ConnectionString;
        }

        partial void CustomizeConfiguration(ref DbContextOptionsBuilder optionsBuilder);

        public virtual DbSet<Category> Categories
        {
            get;
            set;
        }

        public virtual DbSet<OrderProduct> OrderProducts
        {
            get;
            set;
        }

        public virtual DbSet<ProductCategory> ProductCategories
        {
            get;
            set;
        }

        public virtual DbSet<Product> Products
        {
            get;
            set;
        }

        public virtual DbSet<ShipmentOrder> ShipmentOrders
        {
            get;
            set;
        }

        public virtual DbSet<Shipment> Shipments
        {
            get;
            set;
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            this.CategoryMapping(modelBuilder);
            this.CustomizeCategoryMapping(modelBuilder);

            this.OrderProductMapping(modelBuilder);
            this.CustomizeOrderProductMapping(modelBuilder);

            this.ProductCategoryMapping(modelBuilder);
            this.CustomizeProductCategoryMapping(modelBuilder);

            this.ProductMapping(modelBuilder);
            this.CustomizeProductMapping(modelBuilder);

            this.ShipmentOrderMapping(modelBuilder);
            this.CustomizeShipmentOrderMapping(modelBuilder);

            this.ShipmentMapping(modelBuilder);
            this.CustomizeShipmentMapping(modelBuilder);

            RelationshipsMapping(modelBuilder);
            CustomizeMapping(ref modelBuilder);
        }
    
        #region Category Mapping

        private void CategoryMapping(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Category>().ToTable(@"Categories", @"dbo");
            modelBuilder.Entity<Category>().Property<int>(x => x.Id).HasColumnName(@"Id").HasColumnType(@"int").IsRequired().ValueGeneratedOnAdd();
            modelBuilder.Entity<Category>().Property<string>(x => x.Name).HasColumnName(@"Name").HasColumnType(@"varchar(255)").ValueGeneratedNever().HasMaxLength(255);
            modelBuilder.Entity<Category>().HasKey(@"Id");
        }
	
        partial void CustomizeCategoryMapping(ModelBuilder modelBuilder);

        #endregion
    
        #region OrderProduct Mapping

        private void OrderProductMapping(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<OrderProduct>().ToTable(@"OrderProducts", @"dbo");
            modelBuilder.Entity<OrderProduct>().Property<int>(x => x.OrderId).HasColumnName(@"OrderId").HasColumnType(@"int").IsRequired().ValueGeneratedNever();
            modelBuilder.Entity<OrderProduct>().Property<int>(x => x.ProductId).HasColumnName(@"ProductId").HasColumnType(@"int").IsRequired().ValueGeneratedNever();
            modelBuilder.Entity<OrderProduct>().Property<int>(x => x.ShipmentId).HasColumnName(@"ShipmentId").HasColumnType(@"int").IsRequired().ValueGeneratedNever();
            modelBuilder.Entity<OrderProduct>().Property<System.Nullable<int>>(x => x.Quantity).HasColumnName(@"Quantity").HasColumnType(@"int").ValueGeneratedNever();
            modelBuilder.Entity<OrderProduct>().Property<System.Nullable<decimal>>(x => x.Price).HasColumnName(@"Price").HasColumnType(@"money").ValueGeneratedNever();
            modelBuilder.Entity<OrderProduct>().Property<System.Nullable<decimal>>(x => x.Total).HasColumnName(@"Total").HasColumnType(@"money").ValueGeneratedNever();
            modelBuilder.Entity<OrderProduct>().HasKey(@"OrderId", @"ProductId", @"ShipmentId");
        }
	
        partial void CustomizeOrderProductMapping(ModelBuilder modelBuilder);

        #endregion
    
        #region ProductCategory Mapping

        private void ProductCategoryMapping(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<ProductCategory>().ToTable(@"ProductCategories", @"dbo");
            modelBuilder.Entity<ProductCategory>().Property<int>(x => x.ProductId).HasColumnName(@"ProductId").HasColumnType(@"int").IsRequired().ValueGeneratedNever();
            modelBuilder.Entity<ProductCategory>().Property<int>(x => x.CategoryId).HasColumnName(@"CategoryId").HasColumnType(@"int").IsRequired().ValueGeneratedNever();
            modelBuilder.Entity<ProductCategory>().HasKey(@"ProductId", @"CategoryId");
        }
	
        partial void CustomizeProductCategoryMapping(ModelBuilder modelBuilder);

        #endregion
    
        #region Product Mapping

        private void ProductMapping(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Product>().ToTable(@"Products", @"dbo");
            modelBuilder.Entity<Product>().Property<int>(x => x.Id).HasColumnName(@"Id").HasColumnType(@"int").IsRequired().ValueGeneratedOnAdd();
            modelBuilder.Entity<Product>().Property<string>(x => x.Name).HasColumnName(@"Name").HasColumnType(@"varchar(255)").ValueGeneratedNever().HasMaxLength(255);
            modelBuilder.Entity<Product>().Property<string>(x => x.SKU).HasColumnName(@"SKU").HasColumnType(@"varchar(255)").ValueGeneratedNever().HasMaxLength(255);
            modelBuilder.Entity<Product>().Property<string>(x => x.Description).HasColumnName(@"Description").HasColumnType(@"varchar(255)").ValueGeneratedNever().HasMaxLength(255);
            modelBuilder.Entity<Product>().HasKey(@"Id");
        }
	
        partial void CustomizeProductMapping(ModelBuilder modelBuilder);

        #endregion
    
        #region ShipmentOrder Mapping

        private void ShipmentOrderMapping(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<ShipmentOrder>().ToTable(@"ShipmentOrders", @"dbo");
            modelBuilder.Entity<ShipmentOrder>().Property<int>(x => x.OrderId).HasColumnName(@"OrderId").HasColumnType(@"int").IsRequired().ValueGeneratedNever();
            modelBuilder.Entity<ShipmentOrder>().Property<int>(x => x.ShipmentId).HasColumnName(@"ShipmentId").HasColumnType(@"int").IsRequired().ValueGeneratedNever();
            modelBuilder.Entity<ShipmentOrder>().Property<System.Nullable<System.DateTime>>(x => x.DateShipment).HasColumnName(@"DateShipment").HasColumnType(@"datetime").ValueGeneratedNever();
            modelBuilder.Entity<ShipmentOrder>().HasKey(@"OrderId", @"ShipmentId");
        }
	
        partial void CustomizeShipmentOrderMapping(ModelBuilder modelBuilder);

        #endregion
    
        #region Shipment Mapping

        private void ShipmentMapping(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Shipment>().ToTable(@"Shipments", @"dbo");
            modelBuilder.Entity<Shipment>().Property<int>(x => x.Id).HasColumnName(@"Id").HasColumnType(@"int").IsRequired().ValueGeneratedOnAdd();
            modelBuilder.Entity<Shipment>().Property<string>(x => x.Address).HasColumnName(@"Address").HasColumnType(@"varchar(1000)").IsRequired().ValueGeneratedNever().HasMaxLength(1000);
            modelBuilder.Entity<Shipment>().Property<string>(x => x.State).HasColumnName(@"State").HasColumnType(@"varchar(100)").IsRequired().ValueGeneratedNever().HasMaxLength(100);
            modelBuilder.Entity<Shipment>().Property<string>(x => x.City).HasColumnName(@"City").HasColumnType(@"varchar(100)").IsRequired().ValueGeneratedNever().HasMaxLength(100);
            modelBuilder.Entity<Shipment>().Property<string>(x => x.Country).HasColumnName(@"Country").HasColumnType(@"varchar(100)").IsRequired().ValueGeneratedNever().HasMaxLength(100);
            modelBuilder.Entity<Shipment>().HasKey(@"Id");
        }
	
        partial void CustomizeShipmentMapping(ModelBuilder modelBuilder);

        #endregion

        private void RelationshipsMapping(ModelBuilder modelBuilder)
        {

        #region Category Navigation properties

            modelBuilder.Entity<Category>().HasMany(x => x.ProductCategories).WithOne(op => op.Category).IsRequired(true).HasForeignKey(@"CategoryId");

        #endregion

        #region OrderProduct Navigation properties

            modelBuilder.Entity<OrderProduct>().HasOne(x => x.Product).WithMany(op => op.OrderProducts).IsRequired(true).HasForeignKey(@"ProductId");
            modelBuilder.Entity<OrderProduct>().HasOne(x => x.ShipmentOrder).WithMany(op => op.OrderProducts).IsRequired(true).HasForeignKey(@"OrderId", @"ShipmentId");

        #endregion

        #region ProductCategory Navigation properties

            modelBuilder.Entity<ProductCategory>().HasOne(x => x.Category).WithMany(op => op.ProductCategories).IsRequired(true).HasForeignKey(@"CategoryId");
            modelBuilder.Entity<ProductCategory>().HasOne(x => x.Product).WithMany(op => op.ProductCategories).IsRequired(true).HasForeignKey(@"ProductId");

        #endregion

        #region Product Navigation properties

            modelBuilder.Entity<Product>().HasMany(x => x.OrderProducts).WithOne(op => op.Product).IsRequired(true).HasForeignKey(@"ProductId");
            modelBuilder.Entity<Product>().HasMany(x => x.ProductCategories).WithOne(op => op.Product).IsRequired(true).HasForeignKey(@"ProductId");

        #endregion

        #region ShipmentOrder Navigation properties

            modelBuilder.Entity<ShipmentOrder>().HasMany(x => x.OrderProducts).WithOne(op => op.ShipmentOrder).IsRequired(true).HasForeignKey(@"OrderId", @"ShipmentId");
            modelBuilder.Entity<ShipmentOrder>().HasOne(x => x.Shipment).WithMany(op => op.ShipmentOrders).IsRequired(true).HasForeignKey(@"ShipmentId");

        #endregion

        #region Shipment Navigation properties

            modelBuilder.Entity<Shipment>().HasMany(x => x.ShipmentOrders).WithOne(op => op.Shipment).IsRequired(true).HasForeignKey(@"ShipmentId");

        #endregion
        }

        partial void CustomizeMapping(ref ModelBuilder modelBuilder);

        public bool HasChanges()
        {
            return ChangeTracker.Entries().Any(e => e.State == Microsoft.EntityFrameworkCore.EntityState.Added || e.State == Microsoft.EntityFrameworkCore.EntityState.Modified || e.State == Microsoft.EntityFrameworkCore.EntityState.Deleted);
        }

        partial void OnCreated();
    }
}
