﻿using System;
using System.Data;
using System.ComponentModel;
using System.Linq;
using System.Linq.Expressions;
using System.Data.Common;
using System.Collections.Generic;

namespace Orders.Repositories.EF.Models
{
    public partial class ProductCategory {

        public ProductCategory()
        {
            OnCreated();
        }

        public virtual int ProductId
        {
            get;
            set;
        }

        public virtual int CategoryId
        {
            get;
            set;
        }

        public virtual Category Category
        {
            get;
            set;
        }

        public virtual Product Product
        {
            get;
            set;
        }
    
        #region Extensibility Method Definitions

        partial void OnCreated();

        public override bool Equals(object obj)
        {
          ProductCategory toCompare = obj as ProductCategory;
          if (toCompare == null)
          {
            return false;
          }

          if (!Object.Equals(this.ProductId, toCompare.ProductId))
            return false;
          if (!Object.Equals(this.CategoryId, toCompare.CategoryId))
            return false;
          
          return true;
        }

        public override int GetHashCode()
        {
          int hashCode = 13;
          hashCode = (hashCode * 7) + ProductId.GetHashCode();
          hashCode = (hashCode * 7) + CategoryId.GetHashCode();
          return hashCode;
        }
        
        #endregion
    }

}
