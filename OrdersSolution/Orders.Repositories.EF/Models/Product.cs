﻿using System.Data;
using System.ComponentModel;
using System.Linq;
using System.Linq.Expressions;
using System.Data.Common;
using System.Collections.Generic;

namespace Orders.Repositories.EF.Models
{
    public partial class Product {

        public Product()
        {
            this.OrderProducts = new List<OrderProduct>();
            this.ProductCategories = new List<ProductCategory>();
            OnCreated();
        }

        public virtual int Id
        {
            get;
            set;
        }

        public virtual string Name
        {
            get;
            set;
        }

        public virtual string SKU
        {
            get;
            set;
        }

        public virtual string Description
        {
            get;
            set;
        }

        public virtual IList<OrderProduct> OrderProducts
        {
            get;
            set;
        }

        public virtual IList<ProductCategory> ProductCategories
        {
            get;
            set;
        }
    
        #region Extensibility Method Definitions

        partial void OnCreated();
        
        #endregion
    }

}
