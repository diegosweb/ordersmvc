﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography.X509Certificates;
using Microsoft.EntityFrameworkCore.Metadata.Internal;
using Orders.Framework.Contracts.Repositories;
using Orders.Framework.Dtos;
using Orders.Framework.Entities;
using Orders.Repositories.EF.Contexts;
using ModelDatabase = Orders.Repositories.EF.Models;


namespace Orders.Repositories.EF.Repositories
{
    public class OrdersRepository: BaseRepository, IOrdersRepository
    {
        public OrdersRepository(OrdersDBContext context)
            :base(context)
        {}

        #region IOrdersRepository

        public void Add(Order order)
        {
        /*
            List<Orders.Repositories.EF.Models.OrderProduct> dbOrderProducts = new List<Models.OrderProduct>();

            ModelDatabase.Order dbOrder = new ModelDatabase.Order();
            dbOrder.FirstName = order.FirstName;
            dbOrder.LastName = order.LastName;
            dbOrder.Address = order.Address;
            dbOrder.City = order.City;
            dbOrder.State = order.State;
            dbOrder.OrderProducts = new List<ModelDatabase.OrderProduct>();


            foreach (Orders.Framework.Entities.OrderProduct op in order.OrderProducts)
            {
                Models.OrderProduct dbOrderProduct = new Models.OrderProduct();
                dbOrderProduct.OrderId = op.OrderId;
                dbOrderProduct.Price = op.Price;
                dbOrderProduct.ProductId = op.ProductId;
                dbOrderProduct.Quantity = op.Quantity;
                dbOrderProduct.Total = op.Total;
                dbOrder.OrderProducts.Add(dbOrderProduct);
            }

            //Context.Orders.Add(dbOrder);*/
        }

        public void Update(Order order)
        {
          /* var toUpdate = Context.Orders.SingleOrDefault(x => x.Id == order.Id);

            if (toUpdate == null)
                return;

           // toUpdate.FirstName = order.FirstName;
            //toUpdate.LastName = order.LastName;

            Context.OrderProducts.RemoveRange(toUpdate.OrderProducts);

            foreach(Orders.Framework.Entities.OrderProduct orderProduct in order.OrderProducts)
            {
                Orders.Repositories.EF.Models.OrderProduct op = new Models.OrderProduct();
                op.OrderId = orderProduct.OrderId;
                op.ProductId = orderProduct.ProductId;
                Context.OrderProducts.Add(op);
            }*/
        }

        public bool Remove(int id)
        {
           /* var toDelete = Context.Orders.SingleOrDefault(x => x.Id == id);

            if (toDelete == null)
                return false;

            Context.Orders.Remove(toDelete);*/
            return true;
        }


        /*public IEnumerable<OrderDto> GetList()
        {
            var query = Context.Orders.AsQueryable();

            return query
                .OrderBy(x => x.FirstName)
                .ThenBy(x => x.LastName)
                .ToList();

        }*/
        #endregion


        #region Private methods

      private static Order MapToEntity(ModelDatabase.ShipmentOrder dbShipmentOrder)
        {
            /*   Order order = new Order()
             {
                 Id = dbOrder.Id,
                /* FirstName = dbOrder.FirstName,
                 LastName = dbOrder.LastName,
                 Address = dbOrder.Address,
                 City = dbOrder.City,
                 State = dbOrder.State,
            OrderProducts = new List<Orders.Framework.Entities.OrderProduct>()
                // Emails = model.Emails.Select(y => y.Email)
            };

            foreach(ModelDatabase.OrderProduct op in dbOrder.OrderProducts)
            {
                OrderProduct entityOrderProduct = new OrderProduct();
                entityOrderProduct.OrderId = dbOrder.Id;
                entityOrderProduct.ProductId = op.ProductId;
                entityOrderProduct.Quantity = op.Quantity.Value;
                entityOrderProduct.Price = op.Price.Value;
                entityOrderProduct.Total = op.Total.Value;
                order.OrderProducts.Add(entityOrderProduct);
            }*/
            return null;
        }

       /* private static OrderInfoDto MapToDto(OrderModel model)
        {
            return new OrderInfoDto()
            {
                OrderId = model.OrderId,
                FirstName = model.FirstName,
                LastName = model.LastName
            };
        }*/


        public Order GetOrderByID(int id)
        {
            /*var order = Context.Orders.SingleOrDefault(x => x.Id == id);
            order.OrderProducts = Context.OrderProducts.Where(x => x.OrderId == id).ToList<ModelDatabase.OrderProduct>();

            if (order != null)
                return MapToEntity(order);
            else
                return null;*/

            return null;
        }

        OrderDto IOrdersRepository.GetOrderInfo(int id)
        {
            throw new NotImplementedException();
        }

        IEnumerable<OrderDto> IOrdersRepository.GetList()
        {
            throw new NotImplementedException();
        }

        public IEnumerable<OrderDto> GetList()
        {
            throw new NotImplementedException();
        }

        #endregion

    }
}
