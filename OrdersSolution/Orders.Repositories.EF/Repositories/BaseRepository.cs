﻿using Orders.Repositories.EF.Contexts;

namespace Orders.Repositories.EF.Repositories
{
    public abstract class BaseRepository
    {
        public BaseRepository(OrdersDBContext context)
        {
            Context = context;
        }
        protected OrdersDBContext Context { get; private set; }
    }
}
