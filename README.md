# README #


### What is this repository for? ###

* The purpose of this C# Demo is to show some development tools working and design patterns like Factory Pattern and Dependency Injection using Autofac. 
* This solution use Domain Driven Design (D.D.D.) to isolate requirements from techonologies.
* I also demostrate how to use Entity Framework or Dapper for the same Database, just changing only one line of code.
* Version: 1.0.1


### How do I get set up? ###

* Summary of set up
The approach is Database First, so install the .bak file first on a SQL Server Instance

* Configuration: 

* Dependencies: 

* Database configuration

* Deployment instructions

### Contribution guidelines ###

* Writing tests: this project is not added yet, go ahead and add a project for this demo if you want to contribute on this project
* Code review: no body has reviewed this code yet, but you can do it and send me your feedback.


### Who do I talk to? ###

* Repo owner or admin: Ing. Diego Schonhals - dschonhals@hotmail.com
